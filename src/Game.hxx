#pragma once

#include "Map.hxx"


namespace Super_Maruo_Bros {


    /*!
     *
     */
    class Game {
     public:
        Game();
        ~Game();

     public:
        /*!
         *
         */
        static bool     isQuitGame() { return quit_game__; }

        /*!
         *
         */
        static void     toggleQuitGame(bool quit_game) { quit_game__ = quit_game; }
 
        /*!
         *
         */
        static Map *const getMap();

        /*!
         *
         */
        void mainLoop();

     protected:
        /*!
         *
         */
        bool initialize();

        /*!
         *
         */
        void finalize();

     private:
        void onInput();
        void onInputPlayer();
        void onInputMenu();
        void onMouseInput();

     protected:
        static bool                           quit_game__;

     private:
        SDL_Window     *window_;
        SDL_Renderer   *renderer_;
        SDL_Event      *main_event_;

        int32_t         frame_time_;
        uint32_t        fps_time_;
        int32_t         number_of_fps_, fps_;

        enum Direct     first_dir_;

        static       Map           *map__;

        static       bool           move_pressed__, key_menu_pressed__, key_up__, key_down__, key_left__, key_right__, key_shift__;
        static       bool           key_left_pressed__, key_right_pressed__;
        
        static const int32_t        MAIN_FRAME_TIME      = 16;

        enum Direct {
            LEFT      = 0,
            RIGHT     = 1,
        };
    };


}
