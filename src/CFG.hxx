#pragma once

#include "Text.hxx"
#include "MenuManager.hxx"
#include "Music.hxx"

namespace Super_Maruo_Bros {
    /*!
     *
     */
    class CFG {
    public:
        /*!
         *
         */
        static MenuManager *const getMenuManager();

        /*!
         *
         */
        static Music *const getMusic();

        /*!
         *
         */
        static Text *const getText();

    public:
        static const int32_t   GAME_WIDTH = 800;
        static const int32_t   GAME_HEIGHT = 600;

    private:
        static MenuManager *menu_manager__;
    };
}
