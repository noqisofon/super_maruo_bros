#include "top.hxx"

#include "Game.hxx"
#include "CFG.hxx"

#include "AboutMenu.hxx"
using namespace Super_Maruo_Bros;


static int32_t rand_int(int32_t max_value) {
    std::random_device              seed_gen;
    std::mt19937_64                 engine( seed_gen() );

    std::uniform_int_distribution<> dist( 0, max_value );

    return dist( engine );
}

static int32_t rand_int(int32_t min_value, int32_t max_value) {
    std::random_device              seed_gen;
    std::mt19937_64                 engine( seed_gen() );

    std::uniform_int_distribution<> dist( min_value, max_value );

    return dist( engine );
}

AboutMenu::AboutMenu()
    : time_(),
      red_(93), green_(148), blue_(252),
      next_red_(0), next_green_(0), next_blue_(0),
      color_step_id_(1), color_id_(0),
      mode_direction_(true),
      number_of_units_(0) {
    menu_options_.push_back( new MenuOption( "MAIN MENU", 150, 340 ) );
}

AboutMenu::~AboutMenu() {
}

void AboutMenu::update() {
    if ( SDL_GetTicks() >= time_ + 35 ) {
        red_   = getColorStep( red_  , next_red_ );
        green_ = getColorStep( green_, next_green_ );
        blue_  = getColorStep( blue_ , next_blue_ );

        if ( color_step_id_ >= 15 || ( red_ == next_red_ && green_ == next_green_ && blue_ == next_blue_ ) ) {
            nextColor();
            color_step_id_ = 1;
        } else {
            ++ color_step_id_;
        }

        Game::getMap()->setLevelType( rand_int( 4 ) );

        if ( rand_int( 10 ) < 6 ) {
            Game::getMap()->addGoombas( -(int32_t)Game::getMap()->getXPos() + rand_int( CFG::GAME_WIDTH + 128 ),
                                        -32,
                                        rand_int( 2 ) == 0 );
            Game::getMap()->addGoombas( -(int32_t)Game::getMap()->getXPos() + rand_int( CFG::GAME_WIDTH + 128 ),
                                        -32,
                                        rand_int( 2 ) == 0 );
        } else if ( rand_int( 10 ) < 8 ) {
            Game::getMap()->addKoppa( -(int32_t)Game::getMap()->getXPos() + rand_int( CFG::GAME_WIDTH + 128 ),
                                      -32,
                                      0,
                                      rand_int( 2 ) == 0 );
            Game::getMap()->addKoppa( -(int32_t)Game::getMap()->getXPos() + rand_int( CFG::GAME_WIDTH + 128 ),
                                      -32,
                                      0,
                                      rand_int( 2 ) == 0 );
        } else if ( rand_int(  6 ) < 4 ) {
            Game::getMap()->addFire( -Game::getMap()->getXPos() + CFG::GAME_WIDTH,
                                     CFG::GAME_HEIGHT - 16.0f - rand_int( 16 ) * 32,
                                     CFG::GAME_HEIGHT - 16 - rand_int( 16 ) * 32 );
        } else if ( rand_int(  6 ) < 4 ) {
            Game::getMap()->addBulletBill( (int32_t)( Game::getMap()->getXPos() + CFG::GAME_WIDTH ),
                                           CFG::GAME_HEIGHT - 16 - rand_int( 16 ) * 32,
                                           true,
                                           1 );
        } else {
            Game::getMap()->addFireBall( -(int32_t)Game::getMap()->getXPos() + rand_int( CFG::GAME_WIDTH + 128 ) + 8,
                                         CFG::GAME_HEIGHT - 16 - rand_int( 16 ) * 32,
                                         rand_int(   8 ) + 4 + 8,
                                         rand_int( 360 ),
                                         rand_int(   2 ) == 0 );
        }

        updateTime();
    }

    if ( move_direction_ && CFG::GAME_WIDTH - Game::getMap()->getXPos() >= ( (  Game::getMap()->getMapWidth() - 20) * 32) ) {
        move_direction_ = !move_direction_;
    } else if ( !move_direction_ && -Game::getMap()->getXPos() <= 0 ) {
        move_direction_ = !move_direction_;
    }

    Game::getMap()->setXPos( Game::getMap()->getXPos() + 4 * ( move_direction_
                                                               ? -1
                                                               :  1 ) );
}

void AboutMenu::draw(SDL_Renderer *const renderer) {
    CFG::getText()->drawString( renderer, "Maruo v0.0.1 - C++ and SDL2", 150, 128, 0, 0, 0 );
    CFG::getText()->drawString( renderer, "Author: Ned Rihine"         , 150, 146, 0, 0, 0 );

    for ( auto menu_option : menu_options_ ) {
        CFG::getText()->drawString( renderer,
                                    menu_option->getText(),
                                    menu_option->getXPos(),
                                    menu_option->getYPos(),
                                    0, 0, 0 );
    }

    CFG::getMenuManager()->getActiveOption()->draw( renderer,
                                                    menu_options_[active_menu_option_]->getXPos() - 32,
                                                    menu_options_[active_menu_option_]->getYPos() );
}

void AboutMenu::enter() {
    CFG::getMenuManager()->resetActiveOptionId( CFG::getMenuManager()->getMainManu() );
    CFG::getMenuManager()->setViewId( CFG::getMenuManager()->getMainManu() );

    reset();

    CFG::getMusic()->stopMusic();
}

void AboutMenu::launch() {
    red_   =  93;
    green_ = 148;
    blue_  = 252;
}

void AboutMenu::reset() {
    Game::getMap()->setXPos( 0 );
    Game::getMap()->loadLevel();
}

void AboutMenu::nextColor() {
    int32_t color_id =  color_id_;

    while ( color_id == color_id_ ) {
        color_id_ = rand_int( 16 );
    }

    ++ color_id_;

    switch ( color_id_ ) {
    case  0:
        next_red_   =  73;
        next_green_ = 133;
        next_blue_  = 203;
        break;

    case  1:
        next_red_   = 197;
        next_green_ = 197;
        next_blue_  = 223;
        break;

    case  2:
        next_red_   =  27;
        next_green_ =  60;
        next_blue_  = 173;
        break;

    case  3:
        next_red_   =   6;
        next_green_ =  21;
        next_blue_  =  86;
        break;

    case  4:
        next_red_   = 183;
        next_green_ =  85;
        next_blue_  =  76;
        break;

    case  5:
        next_red_   = 110;
        next_green_ =  85;
        next_blue_  =  70;
        break;

    case  6:
        next_red_   =  55;
        next_green_ =  19;
        next_blue_  =  63;
        break;

    case  7:
        next_red_   = 115;
        next_green_ =  53;
        next_blue_  = 126;
        break;

    case  8:
        next_red_   = 115;
        next_green_ =  53;
        next_blue_  = 126;
        break;

    case  9:
        next_red_   = 115;
        next_green_ =  53;
        next_blue_  = 126;
        break;

    case 10:
        next_red_   = 115;
        next_green_ =  53;
        next_blue_  = 126;
        break;

    case 11:
        next_red_   = 115;
        next_green_ =  53;
        next_blue_  = 126;
        break;

    case 12:
        next_red_   = 115;
        next_green_ =  53;
        next_blue_  = 126;
        break;

    case 13:
        next_red_   = 115;
        next_green_ =  53;
        next_blue_  = 126;
        break;

    case 14:
        next_red_   = 115;
        next_green_ =  53;
        next_blue_  = 126;
        break;

    default:
        next_red_   = rand_int( 255 );
        next_green_ = rand_int( 255 );
        next_blue_  = rand_int( 255 );
        break;
    
    }
}

int32_t AboutMenu::getColorStep(int32_t old_color, int32_t new_color) {
    return old_color + ( old_color > new_color
                         ? (old_color - new_color) * color_step_id_ / 30
                         : (new_color - old_color) * color_step_id_ / 30 );
}

void AboutMenu::setBackgroundColor(SDL_Renderer *const renderer) {
    SDL_SetRenderDrawColor( renderer, red_, green_, blue_, 255 );
}

void AboutMenu::updateTime() {
    time_ = SDL_GetTicks();
}
