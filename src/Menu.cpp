#include "CFG.hxx"

#include "Menu.hxx"
using namespace Super_Maruo_Bros;


Menu::Menu() : active_menu_option_(0) {
}

Menu::~Menu() {
}

void Menu::update() {
}

void Menu::updateActiveButton(int32_t direction) {
    switch ( direction ) {
    case SMB_DIRECTION_TOP:
        if ( active_menu_option_ - 1 < 0 ) {
            active_menu_option_   = menu_option_quantity_ - 1;
        } else {
            -- active_menu_option_;
        }
        break;

    case SMB_DIRECTION_BOTTOM:
        if ( active_menu_option_ + 1 >= menu_option_quantity_ ) {
            active_menu_option_   = 0;
        } else {
            ++ active_menu_option_;
        }
        break;

    default:
        break;
    }
}

void Menu::draw(SDL_Renderer *const a_renderer) {
    for ( auto menu_option : menu_options_ ) {
        CFG::getText()->draw( a_renderer, menu_option->getText(), menu_option->getXPos(), menu_option->getYPos() );
    }

    auto active_option      = CFG::getMenuManager()->getActiveOption();
    auto active_menu_option = menu_options_[active_menu_option_];

    active_menu_option->draw( a_renderer, active_menu_option->getXPos() - 32, active_menu_option->getYPos() );
}
