#pragma once

#include <stddef.h>
#include <stdint.h>

#include <cassert>
#include <string>
#include <vector>
#include <random>

#ifdef SMB_USE_NUGET_PACKAGE
#   include <SDL.h>
#   include <SDL_mixer.h>
#   include <SDL_image.h>
#else
#   include <SDL2/SDL.h>
#   include <SDL2/SDL_mixer.h>
#   include <SDL2/SDL_image.h>
#endif
