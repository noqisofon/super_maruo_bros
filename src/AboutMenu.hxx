#pragma once

#include "Menu.hxx"

namespace Super_Maruo_Bros {


    /*!
     *
     */
    class AboutMenu : public Menu {
     public:
        /*!
         *
         */
        AboutMenu();

        /*!
         *
         */
        ~AboutMenu();

     public:
        /*!
         *
         */
        void update();

        /*!
         *
         */
        void draw(SDL_Renderer *const renderer);

        /*!
         *
         */
        void enter();

        /*!
         *
         */
        void launch();

        /*!
         *
         */
        void reset();

        /*!
         *
         */
        void nextColor();

        /*!
         *
         */
        int32_t getColorStep(int32_t old_color, int32_t new_color);

        /*!
         *
         */
        void setBackgroundColor(SDL_Renderer *const renderer);

        /*!
         *
         */
        void updateTime();

     private:
        uint32_t     time_;
        int32_t      red_     , green_     , blue_;
        int32_t      next_red_, next_green_, next_blue_;
        int32_t      color_step_id_, color_id_;
        bool         move_direction_;
        int          number_of_units_;
    };


}
