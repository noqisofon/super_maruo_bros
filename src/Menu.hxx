#pragma once

#include "top.hxx"
#include "MenuOption.hxx"


namespace Super_Maruo_Bros {


#define  SMB_DIRECTION_TOP    0
#define  SMB_DIRECTION_RIGHT  1
#define  SMB_DIRECTION_BOTTOM 2
#define  SMB_DIRECTION_LEFT   3


    /*!
     *
     */
    class Menu {
     public:
        Menu();
        ~Menu();

     public:
        /*!
         *
         */
        virtual void update();
        /*!
         *
         */
        virtual void updateActiveButton(int32_t direction);

        /*!
         *
         */
        virtual void draw(SDL_Renderer *const a_renderer);

     protected:
        int32_t                      active_menu_option_;
        int32_t                      menu_option_quantity_;
        std::vector<MenuOption *>    menu_options_;
    };


}
