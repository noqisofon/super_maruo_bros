#include "top.hxx"

#include "CFG.hxx"
#include "Image.hxx"
#include "Text.hxx"

#include "Game.hxx"
using namespace Super_Maruo_Bros;

bool    Game::quit_game__         = false;
Map    *Game::map__               = NULL;

bool    Game::move_pressed__      = false;
bool    Game::key_menu_pressed__  = false;
bool    Game::key_up__            = false;
bool    Game::key_down__          = false;
bool    Game::key_left__          = false;
bool    Game::key_right__         = false;
bool    Game::key_shift__         = false;

bool    Game::key_left_pressed__  = false;
bool    Game::key_right_pressed__ = false;

Game::Game()
    : window_(NULL), renderer_(NULL), main_event_(NULL),
      frame_time_(0), fps_time_(0), number_of_fps_(0), fps_(0),
      first_dir_(Direct::LEFT) {
    quit_game__ = false;
    
    initialize();
}

Game::~Game() {
    finalize();
}

static bool load_icon_image(SDL_Window *const& window, std::string& filename) {
    SDL_Surface   *loaded_surface = NULL;

    loaded_surface = IMG_Load( filename.c_str() );

    if ( loaded_surface == NULL ) {

        return false;
    }

    SDL_SetColorKey( loaded_surface,
                     SDL_TRUE,
                     SDL_MapRGB( loaded_surface->format, 255, 0, 255 ) );

    SDL_SetWindowIcon( window, loaded_surface );
    SDL_FreeSurface( loaded_surface );

    return true;
}

bool Game::initialize() {
    SDL_Init( SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO );

    window_ = SDL_CreateWindow( "Super Maruo Bros",
                                SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                CFG::GAME_WIDTH, CFG::GAME_HEIGHT,
                                SDL_WINDOW_SHOWN );

    if ( window_ == NULL ) {
        quit_game__ = true;
    }

    renderer_ = SDL_CreateRenderer( window_, -1, SDL_RENDERER_ACCELERATED );

    std::string     filename( "files/images/ico.bmp" );
    load_icon_image( window_, filename );

    main_event_ = new SDL_Event();

    Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 );

    map__ = new Map( renderer_ );

    CFG::getMenuManager()->setActiveOption( renderer_ );
    CFG::getSuperMaruoBrosLogo()->setImage( "super_maruo_bros", renderer_ );
    CFG::getMusic()->playMusic();

    CFG::key_id_up__    = SDLK_UP;
    CFG::key_id_left__  = SDLK_LEFT;
    CFG::key_id_down__  = SDLK_DOWN;
    CFG::key_id_right__ = SDLK_RIGHT;

    CFG::key_id_jump__  = SDLK_SPACE;
    CFG::key_id_dash__  = SDLK_LSHIFT;

    return true;
}

void Game::finalize() {
    delete map__;
    delete main_event_;

    SDL_DestroyRenderer( renderer_ );
    SDL_DestroyWindow( window_ );
}

void Game::mainLoop() {
    fps_time_ = SDL_GetTicks();

    while ( !isQuitGame() && main_event_->type != SDL_QUIT ) {
        frame_time_ = SDL_GetTicks();

        SDL_PollEvent( main_event_ );
        SDL_RenderClear( renderer_ );

        CFG::getMenuManager()->setBackgroundColor( renderer_ );
        SDL_RenderFillRect( renderer_, NULL );

        onInput();
        onMouseInput();
        update();
        draw();

        SDL_RenderPresent( renderer_ );

        if ( SDL_GetTicks() - frame_time_ < MIN_FRAME_TIME ) {
            SDL_Delay( MIN_FRAME_TIME - ( SDL_GetTicks() - frame_time_ ) );
        }
    }
}
